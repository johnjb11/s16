let students = ["TJ", "Mia", 'Tin', "Chris"];

console.log(students[0]);
console.log(students[1]);
console.log(students[2]);
console.log(students[3]);


/*While Loop*/
let count = 5; 
while(count !== 0){ 
	console.log("Sylvan");
	count--; 
	
}


let number = 1;

while(number <= 5){
	console.log(number);
	number++;
}

let fruits = ["Banana", "Mango"];
let indexNumber = 0;


while (indexNumber <= 1) {
	console.log(fruits[indexNumber]);
	indexNumber++;
}


let mobilePhones = ['Samsung Galaxy S21', 'Iphone 13 Pro', 'Xiaomi 11T', 'Realme C', 'Huawei Nova 8', 'Pixel 5', 'ASUS ROG 6'];

console.log(mobilePhones.length);
console.log(mobilePhones.length - 1);
console.log(mobilePhones[mobilePhones.length - 1]);
console.log(mobilePhones[2]);

let indexNumberForMobile = 0;

while(indexNumberForMobile <= mobilePhones.length - 1){
	console.log(mobilePhones[indexNumberForMobile]);
	indexNumberForMobile++;
}

let countA = 1;

do {
	console.log("Juan");
	countA++
} while(countA <=6);


console.log("=====Do-while VS While=====");



let countB = 6;

do{
	console.log(`Do-while count ${countB}`);
	countB--;
} while(countB == 7); /*the statement prints because it "do" the first action (the console log) before the condition*/

/*versus*/

while(countB == 7){
	console.log(`While count ${countB}`);
	countB--;
}

let indexNumberA = 0;

let computerBrands = ['Apple Macbook ', 'HP NoteBook', 'Asus', 'Lenovo', 'Acer', 'Dell', 'Huawei'];

do{
	console.log(computerBrands[indexNumberA]);
	indexNumberA++;

}while(indexNumberA <= [computerBrands.length - 1]);






/*for loop*/
for(let count = 5; count >= 0; count--){
	console.log(count);
}

/*mini activity*/
let colors = ['Red', 'Green', 'Blue', 'Yellow', 'Purple', 'White', 'Black'];


for(let colorCount = 6; colorCount >= 0; colorCount--){
	console.log(colors[colorCount]);
}

/*continue - skip a block of code then continue the next iteration*/
/*break - stops the execution of the code*/

let ages = [18, 19, 29, 21, 24, 25];

for(let i = 0; i <= ages.length -1; i++){
	if(ages[i] == 21 || ages[i] == 18){
		continue;
	}
	console.log(ages[i]);
}

let studentNames = ['Den', 'Jayson', 'Marvin', 'Rommel' ];
for (let i = 0; i <= studentNames.length -1; i++){
	if(studentNames[i] == "Jayson"){

console.log(studentNames[i]);
break;
}
}







/*PUSH TO GITLAB*/


/*activity 1*/



let adultAge = [20, 23, 33, 27, 18, 19, 70, 15, 55, 63, 85, 12, 19];

for(let belowTwenty = 0; belowTwenty <= adultAge.length-1; belowTwenty++){
	if(adultAge[belowTwenty] <= 19){
		continue;
	}
	console.log(adultAge[belowTwenty]);
}




/*activity 2*/


let student2 = ['Gary', 'Amelie', 'Anne', 'Jazz', 'Preina', 'James', 'Kelly', 'Diane', 'Lucy', 'Vanessa', 'Kim', 'Francine']

function searchStudent(studentName){
	for (let indexStudentNumber = 0; indexStudentNumber <= student2.length -1; indexStudentNumber++){
	if(student2[indexStudentNumber] == studentName){
		console.log(student2[indexStudentNumber]);
break;
}
}
}

searchStudent('Francine');